import FetchKey, { FetchLiteOptions, KeyError } from './main.js';
import { combined, separate } from './fixtures.js';

const headers = {
	get(){ return 'application/activity+json'; }
}

const mockFetch = async function(url: string, options: FetchLiteOptions){
	if(!options.headers.accept.split(/,\s*/).includes('application/ld+json;profile="http://www.w3.org/ns/activitystreams"')) {
		throw new Error('Library uses incorrect accept header');
	}

	if(url === 'ex:404') {
		return {
			ok: false,
			status: 404,
			async text() {
				return 'Page not found'
			},
			async json() {
				return JSON.parse(await this.text())
			},
			headers
		}
	}

	if(url === 'ex:invalid') {
		return {
			ok: true,
			status: 200,
			async text() { return ''; },
			async json() {
				return JSON.parse(await this.text())
			},
			headers
		}
	}

	return {
		ok: true,
		async json(){
			if(url === 'ex:Paul#main-key') {
				return combined;
			}
			if(url === 'ex:Paul/key') {
				return separate.key;
			}
			if(url === 'ex:Paul/actor') {
				return separate.actor;
			}
		},
		headers
	}
}

const getKey = new FetchKey(mockFetch);

test('fedi-get-key: combined', async () => {
	expect(await getKey.get('ex:Paul#main-key')).toEqual({
		key: '-----BEGIN PUBLIC KEY-----\nFAKE_KEY\n-----END PUBLIC KEY-----\n',
		owner: combined
	});
});

test('fedi-get-key: separate', async () => {
	expect(await getKey.get('ex:Paul/key')).toEqual({
		key: '-----BEGIN PUBLIC KEY-----\nFAKE_KEY\n-----END PUBLIC KEY-----\n',
		owner: separate.actor
	});
});

test('fedi-get-key: invalid', async () => {
	await expect(getKey.get('ex:invalid')).rejects.toThrow();
});

test('fedi-get-key: 404', async () => {
	await expect(getKey.get('ex:404')).rejects.toThrow(new KeyError(
		'Response status is 404',
		{
			url: 'ex:404',
			status: 404,
			content: 'Page not found'
		}
	));
});

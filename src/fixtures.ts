export const combined = {
	"@context": [
		"https://www.w3.org/ns/activitystreams",
		"https://w3id.org/security/v1",
		"ex:other"
	],
	"id": "ex:Paul",
	"publicKey": {
		"id": "ex:Paul#main-key",
		"owner": "ex:Paul",
		"publicKeyPem": "-----BEGIN PUBLIC KEY-----\nFAKE_KEY\n-----END PUBLIC KEY-----\n"
	}
}

export const separate = {
	key: {
		"@context": [
			"https://www.w3.org/ns/activitystreams",
			"https://w3id.org/security/v1"
		],
		"id": "ex:Paul/key",
		"owner": "ex:Paul/actor",
		"publicKeyPem": "-----BEGIN PUBLIC KEY-----\nFAKE_KEY\n-----END PUBLIC KEY-----\n"
	},
	actor: {
		"@context": [
			"https://www.w3.org/ns/activitystreams",
			"https://w3id.org/security/v1"
		],
		"id": "ex:Paul/actor",
		"publicKey": 'ex:Paul/key'
	}
}

import jld, { JsonLdDocument } from 'jsonld';
import ctx from './ctx.js';

const urlInCtx = (url: string): url is keyof typeof ctx => url in ctx;

export async function staticDocumentLoader(url: string) {
	const document = urlInCtx(url) ? ctx[url] : {};

	return {
		documentUrl: url,
		document,
	};
}

export default async function expand(document: JsonLdDocument) {
	return jld.expand(document, { documentLoader: staticDocumentLoader });
}

declare module 'ld-query' {
    export type QueryReturn = QueryNode | string | number | boolean | QueryReturn[];

    export interface QueryNode {
        query(q: string): QueryReturn | null
        queryAll(q: string): QueryReturn[]
        json(): unknown
    }

    interface Context {
        (data: unknown): QueryNode
    }

    interface LD {
        (ctx: Record<string, string>): Context
        (data: unknown, ctx: Record<string, string>): QueryNode
    }

    const ld: LD;
    export default ld;
}

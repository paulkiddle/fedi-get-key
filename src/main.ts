/// <reference path="./ld-query.d.ts" />
import expand from "./expander.js";
import ldq, { QueryNode, QueryReturn } from 'ld-query';

const context = {
    '@vocab': 'https://w3id.org/security#'
}

export type FetchLiteOptions = {
    headers: {
        accept: string
    }
}

type FetchLiteResponseBase = {
    ok: boolean,
    headers: {
        get(name: string): string | null
    },
    json(): Promise<unknown>
}
type FetchLiteResponseExtras = {
    status: number;
    text(): Promise<string>;
}

export type FetchLiteResponse =  FetchLiteResponseBase | (FetchLiteResponseBase & FetchLiteResponseExtras);

export type FetchLite = (url: string, options: FetchLiteOptions) => Promise<FetchLiteResponse>;

type RequestMeta = {
    url: string,
    status: number | string,
    content: string | null
}

export class KeyError extends Error {
    meta

    constructor(message: string, meta?: RequestMeta) {
        super(message);
        this.meta = meta;
    }
}

function assert (value: boolean | undefined, message: string, meta?: RequestMeta): asserts value is true {
    if(!value) {
        throw new KeyError(message, meta);
    }
}

function assertString(value: unknown, message: string): asserts value is string {
    assert(typeof value === 'string', message);
}


function assertIsRecord(value: unknown, message: string): asserts value is Record<string, unknown> {
    assert(value instanceof Object && !Array.isArray(value), message);
}

function assertIsQueryNode(value: QueryReturn | null, message: string): asserts value is QueryNode {
    assert((value instanceof Object) && ('query' in value), message);
}


class LinkedDataFetcher {
    #fetch: FetchLite

    constructor(fetch: FetchLite) {
        this.#fetch = fetch;
    }

    async get(url: string) {
        const res = await this.#fetch(url, {
            headers: {
                accept: 'application/ld+json;profile="http://www.w3.org/ns/activitystreams", application/activity+json, application/ld+json;q=0.9, application/json;q=0.8'
            }
        });

        const { content, json, status } = 'status' in res ? {
            content: await res.text(),
            json: () => JSON.parse(content!) as unknown,
            status: res.status
        } : {
            content: null,
            status: 'Not OK',
            json: () => res.json()
        };

        const meta = { url, content, status };

        assert(res.ok, `Response status is ${status}`, meta);
        assert(res.headers.get('content-type')?.includes('json'), 'Response content type is not json.', meta);

        const body = await json();

        assertIsRecord(body, `Response body is not an object.`)

        const expanded = await expand(body);
        const q = ldq(expanded, context);

        return Object.assign(q, {
            document: body
        });
    }
}

class GetKey {
    #ldfetch

    constructor(ldf: LinkedDataFetcher) {
        this.#ldfetch = ldf;
    }

    async get(url: string) {
        const q = await this.#ldfetch.get(url);
        const key = q.query(`#${url}`);

        assertIsQueryNode(key, 'Public key is not an object');

        const ownerId = key.query('owner @id');

        assertString(ownerId, 'Document has no owner id string');

        const owner = ownerId.split('#')[0] !== url.split('#')[0] ? await this.#ldfetch.get(ownerId) : q;
        assert(owner.queryAll('publicKey @id').includes(url), 'The owner of the public key does not list that key in their profile');

        const pem = key.query('publicKeyPem @value');

        assertString(pem, 'Key object has no publicKeyPem field, or it is not a string');

        return {
            key: pem,
            owner: owner.document
        }
    }
}


export default class FetchKey extends GetKey {
    constructor(fetch: FetchLite) {
        super(new LinkedDataFetcher(fetch));
    }
}
